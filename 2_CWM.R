library(TreeCo) #Returns the sd for CWM
library(tidyr)
library(dplyr)
library(readxl)

# Calculating the community-weighted trait means (CWM) for spiders, vascular plants, and moss communities
# CWM can be used to determine average community trait values and explore what traits dominate a site or sampling unit

# subset the Vegetation data into 2 groups: vascular plants and mosses

vascular <- c("Agr.can",
              "Agr.cap",
              "Agr.sto",
              "Aln.sp.",
              "Ane.nem",
              "Ant.odo",
              "Ath.fil",
              "Bet.pub",
              "Cal.aru",
              "Cal.can",
              "Cal.vul",
              "Cal.pal",
              "Car.bru",
              "Car.dem",
              "Car.dio",
              "Car.ech",
              "Car.nig",
              "Car.lep",
              "Car.pan",
              "Car.ros",
              "Car.vag",
              "Car.ves",
              "Cha.ang",
              "Cir.het",
              "Com.pal",
              "Cor.ave",
              "Dac.mac",
              "Des.ces",
              "Ave.fle",
              "Emp.nig",
              "Equ.flu",
              "Equ.syl",
              "Fes.ovi",
              "Fil.ulm",
              "Fra.ves",
              "Gal.tet",
              "Gal.pal",
              "Gly.flu",
              "Gym.dry",
              "Hyp.per",
              "Jun.art",
              "Jun.con",
              "Jun.eff",
              "Jun.fil",
              "Jun.com",
              "Lac.mur",
              "Lin.bor",
              "Luz.mul",
              "Luz.pil",
              "Lyc.ann",
              "Lys.thy",
              "Mel.pra",
              "Mel.syl",
              "Mel.nut",
              "Men.tri",
              "Mai.bif",
              "Mil.eff",
              "Mol.cae",
              "Mon.uni",
              "Oxa.ace",
              "Peu.pal",
              "Pha.aru",
              "Phe.con",
              "Pic.abi",
              "Pin.syl",
              "Poa.fle",
              "Pot.gra",
              "Pot.ere",
              "Ran.rep",
              "Rub.cha",
              "Rub.ida",
              "Rub.sax",
              "Sen.syl",
              "Sci.syl",
              "Sol.vir",
              "Sor.auc",
              "Suc.pra",
              "Lys.eur",
              "Vac.myr",
              "Vac.vit",
              "Vac.uli",
              "Vac.oxy",
              "Val.sam",
              "Ver.off",
              "Vic.cra",
              "Vio.epi",
              "Vio.pal"
)
moss <- c("Amb.ser",
          "Aul.pal",
          "Bra.rut",
          "Rho.ros",
          "Dic.cri",
          "Dic.maj",
          "Dic.mon",
          "Dic.pol",
          "Dic.sco",
          "Gri.har",
          "Rac.fas",
          "Rac.het",
          "Hyl.spl",
          "Ple.sch",
          "Rhy.squ",
          "Hyp.cup",
          "Pti.cri",
          "Mni.hor",
          "Pla.aff",
          "Pol.com",
          "Pol.jun",
          "Pol.pil",
          "Tor.sub",
          "Sph.cap",
          "Sph.gir",
          "Sph.pal",
          "Tet.pel",
          "Thu.tam",
          "Cal.mue",
          "Lop.lon",
          "Chi.pal",
          "Pla.asp",
          "Pti.cil",
          "Pti.pul"
)
# Load coverage data for vascular plants and mosses

Veg_vascular_Site <- data.frame(read_excel("Vegetation_Data_2020.xlsx", # Load vascular vegetation coverage data
                                           sheet = "SpeciesSite",
                                           col_names = T)) %>% 
  column_to_rownames("Site.Unit") %>% 
  select(all_of(vascular))
Veg_vascular_Site <- Veg_vascular_Site[ , order(names(Veg_vascular_Site))]


Veg_moss_Site <- data.frame(read_excel("Vegetation_Data_2020.xlsx", # Load moss coverage data
                                       sheet = "SpeciesSite",
                                       col_names = T)) %>% 
  column_to_rownames("Site.Unit") %>% 
  select(all_of(moss))
Veg_moss_Site <- Veg_moss_Site[ , order(names(Veg_moss_Site))]

# Load vegetation traits

# Vascular plant traits
Veg_Traits_Vascular_CWM <- read_excel("Vegetation_Data_2020.xlsx",   # Vascular Plant traits
                                  sheet = "TraitVasc",
                                  col_names = T) %>% 
  column_to_rownames("Species") %>% 
  dplyr::select(!c(Full_Species:Temp_opt, Max_Height:LDMC_LEDA)) %>% 
  as.data.frame()


# Moss traits
Veg_Traits_Moss_CWM <- read_excel("Vegetation_Data_2020.xlsx",   # Moss traits
                              sheet = "TraitMoss",
                              col_names = T) %>% 
  column_to_rownames("Species") %>% 
  select(!c(Full_Species:Family, Forest_occ:shoot_length)) %>% 
  as.data.frame()

## Community Weighted mean (CWM) calculations ##

# CWM Vascular Plants by SITE
CWM_vascular_site <- functcomp.treeco(Veg_Traits_Vascular_CWM, Veg_vascular_Site,
                                 CWM.type = "all",
                                 CWM.sd = T) %>% 
  rownames_to_column((var = "Site"))

# CWM Vascular Plants by UNIT
CWM_vascular_unit <- functcomp.treeco(Veg_Traits_Vascular_CWM, Veg_vascular_Unit,
                                      CWM.type = "all",
                                      CWM.sd = T)

# CWM Mosses by SITE
CWM_moss_site <- functcomp.treeco(Veg_Traits_Moss_CWM, Veg_moss_Site,
                             CWM.type = "all",
                             CWM.sd = T) %>% 
  rownames_to_column(var = "Site")

# CWM Mosses by UNIT
Veg_moss_Unit <- data.frame(read_excel("Vegetation_Data_2020.xlsx", # Load moss coverage data
                                       sheet = "SpeciesUnit",
                                       col_names = T)) %>% 
  column_to_rownames("Unit") %>% 
  select(all_of(moss))

Veg_moss_Unit <- Veg_moss_Unit[ , order(names(Veg_moss_Unit))]

CWM_moss_unit <- functcomp.treeco(Veg_Traits_Moss_CWM, Veg_moss_Unit,
                           CWM.type = "all",
                           CWM.sd = F) %>% 
  rownames_to_column(var = "Unit") %>% 
  rename(L = ind_L,
         M = ind_F,
         pH = ind_R,
         N = ind_N)
write.csv(CWM_moss_unit,
          "C:/Users/jeffmark/Local Business/Functional Diversity Project/riparian-spider-functional-diversity/Supplementary Material/Moss_CWM.csv", 
          row.names = F)

# CWM for spiders by Site
# This is for max body size and to calculate the proportions of ballooning/non-ballooning spiders at each site

Spi_Traits_CWM <- read_excel("Spider_Function_2020.xlsx",
                                    sheet = "SpiFunc",
                                    col_names = T) %>%
  dplyr::filter(!Species %in% c("Mecynargus paetulus", "Mughiphantes whymperi", "Nigma puella")) %>%
  dplyr::select(!c(Species, guild, humi.vdry:ligh.dark)) %>%
  as.data.frame() %>% 
  column_to_rownames(var = "Spe")
rownames(Spi_Traits_CWM) <- colnames(Spi_Abn_Site)

CWM_spi_site <- functcomp.treeco(Spi_Traits_CWM, Spi_Abn_Site,
                                 CWM.type = "all",
                                 CWM.sd = T) %>% 
  rownames_to_column((var = "Site"))


# CWM for spiders by Unit
Spi_Traits_CWM <- order(Spi_Traits_CWM)
CWM_spi_UNIT <- functcomp.treeco(Spi_Traits_CWM, Spi_Abn_UNIT,
                                 CWM.type = "all",
                                 CWM.sd = T)
CWM_spi_UNIT <- CWM_spi_UNIT %>% 
  rownames_to_column(var ="Full.Unit")


