This repository contains R code and data sets, used to investigate the functional diversity of riparian spiders in Värmland and Örebro counties in Sweden

R code should be run in the order based on the first number in the file name. 0_Null_Model_Modifications.R is a modified version of the R code from the 'fundiv' package in R available at https://github.com/ibartomeus/fundiv

File descriptions:

- **0_Null_Model_Mod_Functions**: Modificatons of two functions found in the fundiv package (https://github.com/ibartomeus/fundiv). Functionsl 'null.FD' and 'FDindexes' were modified to change the dendrogram cluster method, removed functional richness and functional diversity standaradization defaults, added a maximum number of traits to use for PCoA, and added an argument to pass trait weights onto other functions. **This code must be run prior to running the null models in Code files 3 and 4**

- **1_Trait_Weights.R**: R code to determine the optimal solution for spider and vascular plant trait weights using an iterative genetic algorithm (see: Bello et al., 2021). These weights were then passed on to other code blocks to determine different functional diversity measures.

- **2_CWM.R**: R code to calculate the Community Weighted Trait Means (CWMs) for spiders and vascular plant communities.

- **3_Functional_Diversity_Spiders.R**: R Code to calculate functional richness and run null models for the spider communities within each sampling unit. This code relies on code files 0 and 1 to run properly.

- **4_Functional_Diversity_Vegetation.R**: R code to calculate functional richness and run null models for the vascular plant communities within each sampling unit. This code relies on the code files 0 and 1 to run properly.

- **5_Functional_Redundancy.R**: R code to calculate the functional redundancy for spider and vascular plant communites within each sampling unit. This code relies on the function 'uniqueness' found in the supplementary material from Ricotta et al., 2016.

- **6_SEM.R**: R code to create local models and build Structural Equation Models for both spiders and vascular plants.

- **7_NMDS.R**: Run non-metric multidimentional scaling ordination and indicator species on spider and vascular plant data. Create NMDS plots.


**Data folder:

- **All_data_V3.xlsx**: Comprehensive site data with diversity calculations for each sampling unit.

- **Spider_Data_2020.xlsx**: Spider abundance and richness data for each site and sampling unit. Multiple worsheets break the data down into difference combinatons of Site, sampling unit, species vs. family abundance, and with and with out juveniles. The R codes above are set to pull data from the correct worksheet.

- **Spider_Function_2020.xlsx**: Spider functional trait matrix used to calculate functional richness and functional redundancy.

- **Vegetation_Data_2020.xlsx**: Vascular plant functional trait matrix used to calculate functional richness and functional redundancy.



